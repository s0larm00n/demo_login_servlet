<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Страница успешного входа в систему</title>
</head>
<body>
<%
//allow access only if session exists
String user = (String) session.getAttribute("user");
String userName = null;
String sessionID = null;
Cookie[] cookies = request.getCookies();
if(cookies !=null){
for(Cookie cookie : cookies){
	if(cookie.getName().equals("user")) userName = cookie.getValue();
	if(cookie.getName().equals("JSESSIONID")) sessionID = cookie.getValue();
}
}
%>
<h3>Привет, <%=userName %>, Вы успешно залогинились. ID Вашей сессии = <%=sessionID %></h3>
<br>
User=<%=user %>
<br>

<form action="checkout" method="post">
<input type="submit" value="Проверочная страница" >
</form>

<form action="logout" method="post">
<input type="submit" value="Выход" >
</form>
</body>
</html>