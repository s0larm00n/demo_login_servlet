package com.epam.demologin;


import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Locale;
import java.util.Properties;

import static com.epam.demologin.LoginServlet.openProp;

public class SecurityFilter implements Filter {

    private ServletContext context;

    public void init(FilterConfig fConfig) throws ServletException {
        this.context = fConfig.getServletContext();
        this.context.log("AuthenticationFilter initialized");
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        Locale loc=request.getLocale();
        Properties property=new Properties();

        if(loc.toString().equals("en_US")) {
            openProp(property, "D:\\dev\\logindemoservlet\\src\\main\\webapp\\en.properties");
        }
        else{
            openProp(property, "D:\\dev\\logindemoservlet\\src\\main\\webapp\\ru.properties");
        }

        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;

        String uri = req.getRequestURI();
        this.context.log("Requested Resource::"+uri);

        HttpSession session = req.getSession(false);

        if((session == null ||
                session.getAttribute("role")==null||session.getAttribute("role").equals(""))
                && !(uri.endsWith("login") ||uri.endsWith("login_en.jsp")||uri.endsWith("login_ru.jsp") || uri.endsWith("LoginServlet"))){
            this.context.log("Unauthorized access request");
            res.sendRedirect(property.getProperty("loginpage"));
        }else{
            // pass the request along the filter chain
            chain.doFilter(request, response);
        }


    }

    public void destroy() {
        //close any resources here
    }

}