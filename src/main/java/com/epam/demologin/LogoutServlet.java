package com.epam.demologin;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.Locale;
import java.util.Properties;

import static com.epam.demologin.LoginServlet.openProp;

public class LogoutServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Locale loc=request.getLocale();
        Properties property=new Properties();

        if(loc.toString().equals("en_US")) {
            openProp(property, "D:\\dev\\logindemoservlet\\src\\main\\webapp\\en.properties");
        }
        else{
            openProp(property, "D:\\dev\\logindemoservlet\\src\\main\\webapp\\ru.properties");
        }

        response.setContentType("text/html");
        Cookie[] cookies = request.getCookies();
        if(cookies != null){
            for(Cookie cookie : cookies){
                if(cookie.getName().equals("JSESSIONID")){
                    System.out.println("JSESSIONID="+cookie.getValue());
                    break;
                }
            }
        }
        //invalidate the session if exists
        HttpSession session = request.getSession(false);
        System.out.println(property.getProperty("usr")+"="+session.getAttribute("user"));
        if(session != null){

            session.removeAttribute("role");
            session.removeAttribute("user");

            session.invalidate();
        }
        response.sendRedirect(property.getProperty("loginpage"));
    }

}