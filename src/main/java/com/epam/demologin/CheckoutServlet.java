package com.epam.demologin;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Locale;
import java.util.Properties;

import static com.epam.demologin.LoginServlet.openProp;

public class CheckoutServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {

        Locale loc=request.getLocale();
        Properties property=new Properties();

        if(loc.toString().equals("en_US")) {
            openProp(property, "D:\\dev\\logindemoservlet\\src\\main\\webapp\\en.properties");
        }
        else{
            openProp(property, "D:\\dev\\logindemoservlet\\src\\main\\webapp\\ru.properties");
        }


        response.sendRedirect(property.getProperty("checkoutpage"));
    }
}
