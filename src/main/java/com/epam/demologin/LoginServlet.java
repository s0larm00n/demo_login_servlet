package com.epam.demologin;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Locale;
import java.util.Properties;

public class LoginServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private final String userID = "admin";
    private final String password = "password";

    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {

        Locale loc=request.getLocale();
        Properties property=new Properties();

        if(loc.toString().equals("en_US")) {
            openProp(property, "D:\\dev\\logindemoservlet\\src\\main\\webapp\\en.properties");
        }
        else{
            openProp(property, "D:\\dev\\logindemoservlet\\src\\main\\webapp\\ru.properties");
        }

        String user = request.getParameter("user");
        String pwd = request.getParameter("pwd");

        if(userID.equals(user) && password.equals(pwd)){
            HttpSession session = request.getSession();
            session.setAttribute("user", "My_Dear_Admin");
            session.setAttribute("role", "administrator");

            session.setMaxInactiveInterval(30*60);

            Cookie userName = new Cookie("user", user);
            userName.setMaxAge(30*60);
            response.addCookie(userName);
            response.sendRedirect(property.getProperty("loginsuccess"));
        }else{

            RequestDispatcher rd = getServletContext().getRequestDispatcher("/"+property.getProperty("loginpage"));
            PrintWriter out= response.getWriter();

            String convertedMessage = new String(property.getProperty("wrongpass").getBytes("UTF-8"),"UTF-8");

            out.println("<font color=red>"+convertedMessage+"</font>");
            out.println(loc);
            rd.include(request, response);
        }

    }

    public static void openProp(Properties property,String fname) {
        FileInputStream fis;
        try {
            fis = new FileInputStream(fname);
            property.load(fis);

        } catch (IOException e) {
            System.err.println("Error: missing file!");
        }
    }
}
